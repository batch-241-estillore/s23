// console.log("Hello World");

//OBJECTS
/*
	-An object is a data type that is used to represent real world objects
*/

//Creating objects through objct intitializers / leteral notation

/*
	This declares an object and also intitializes  its values upon creation
	Syntax:
		let/const objectName = {
			keyA: valueA,
			keyB: ValueB
		}
*/


let cellphone = {
	name: 'Nokie 3310',
	manufactureDate: 1999
};
console.log("Result from object initialization: ");
console.log(cellphone);
console.log(typeof cellphone);
console.log("")

//Creating objects using constructor function
/*
	Creates a reusable function to create several objects that have the same data structure
	Useful for creating multiple instances/copies of an object

	Syntax:
		function ObjectName(kayA, keyB){
			this.keyA = keyA,
			this.keyB = valueB
		}
*/

function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}

function Student(firstName, lastName, yearsOld, city){
	this.name = firstName + " " + lastName;
	this.age = yearsOld;
	this.residence = city;

}
/*
	the "this" keyword allows us to assign a new object's properties by associating them with the values received from a constructor function
*/
//This is an instance of the laptop object:
let laptop_gab = new Laptop('Lenovo',2008);
console.log(laptop_gab);

let laptop_john = new Laptop('ASUS', 2021);
console.log(laptop_john);


console.log("This is a unique instance of the Student object:")
let john = new Student('John', 'Est', 61, 'Jupiter');
console.log(john);

console.log('Result from creating objects without the "new" keyword')
let gab = Student('gab', 'est', 41, 'Moon');
console.log(gab)


//Accessing Object Properties
/*
	1.Using the dot notation
	2.Using the square bracket []
		square brackets are usually used if the keyName is not one word thus not allowing the dot notation
*/

//Accessing properties using dot notation
console.log(laptop_john.name);
console.log(john.name)
console.log(john.firstName);//does not work with parameters of the constructor function

//Using the "square bracket" notation
console.log("Result from square bracekt notation: " + laptop_john['name']);
console.log("Result from square bracekt notation: " + john['name']);

let studentLaptop = laptop_john.name;
console.log(studentLaptop);


//Initializing/ Adding / Delete / Reassigning Object Properties

/*
	Like any other variable in JS, objects may have their own properties intialized after the object was created/declared
	This is useful for times when an object's properties are undetermined at the time of creation
*/

//Empty Object
console.log("Empty 'car' Object");
let car = {};
console.log(car);
console.log("");



car.name = "Honda Civic";
car.driver = "Mr. Bean";
console.log("Result from adding properties using dot notation: ");
console.log(car);

//Adding property using bracket notation
car['manufactureDate'] = 2019;
console.log("Result from adding properties using bracket notation: ")
console.log(car['manufactureDate']);
console.log(car);


// Deleting Object Properties
delete car['manufactureDate'];
console.log("Result from deleting properties:");
console.log(car);

//Reassign object properties
car.name = 'Dodge Charger R/T';
console.log("Result from reassigning properties: ");
console.log(car);


//Object Methods
/*
	A method is a function attached to an object as a property
*/


let person= {
	name: 'John',
	talk: function(){
		console.log("Hello, my name is " + this.name)
	}
}

console.log(person);
console.log("Result from object methods:")
person.talk();

person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.');
};

person.walk();


let friend = {
	firstName: 'John',
	lastName: 'Gab',
	address: {
		city: 'City',
		country: 'Country'
	},
	emails: ['johngab@friendster.com','johngab@email.com'],
	introduce: function(classmate){
					console.log(`Hello, my name is ${this.firstName}. Nice to meet you ${classmate}.`)
	}
}
friend.introduce('Eric');
friend.introduce('JD');
console.log("")

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(targetPokemon){
		console.log(this.name + " tackled" + targetPokemon);
		console.log(targetPokemon + "'s health is reduced to " + "targetPokemon" + "'s health")
	},
	faint: function(){
		console.log("Pokemon Fainted");
	}
}

console.log(myPokemon);
myPokemon.tackle('Charmander');
myPokemon.tackle('Arceus');
console.log("");



function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name}'s health is now reduced to ${target.health}`);
	};
	this.faint = function(){
		console.log(this.name + 'fainted')

	}
}

let pikachu = new Pokemon('Pikachu',16);
let ratata = new Pokemon('Ratata',8);

pikachu.tackle(ratata);













